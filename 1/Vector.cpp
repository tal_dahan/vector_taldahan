#include <iostream>
#include "Vector.h"
#include <stdio.h>
#include <stdlib.h>

#define  ERORR_VALUE -9999

using namespace std;
/*
Function create arr(len - n or len - 2) and set the first values of the arguments in the class
input:n-size
output:non
*/
Vector::Vector(int n)
{
	/*check if the n is valid size */
	if (n >= 2)
	{
		/* malloc()- allocate the memory for n integers  (containing garbage values) */
		this->_elements = (int*)malloc(sizeof(int)*n);

		/*set the argument valus*/
		this->_size = 0;
		this->_resizeFactor = n;
		this->_capacity = n;
	}
	else
	{
		/*if n <2*/
		/* malloc()- allocate the memory for 2 integers  (containing garbage values) */
		this->_elements = (int*)malloc(sizeof(int)*2);
		/*set the argument valus*/
		this->_size = 0;
		this->_resizeFactor = 2;
		this->_capacity = 2;
	}
}

/*release all the dynamic memory
intput:non
output:non
*/
Vector::~Vector()
{
	delete[] this->_elements;
	this->_elements = nullptr;
}

/*Function return the size of the vector
input:non
output: _size- size of the vector
*/
int Vector::size()const
{
	return this->_size;
}

/*Function return the capacity of the vector
input:non
output: _capacity
*/
int Vector::capacity()const
{
	return this->_capacity;
}

/*Function return the sizing factor of the vector
input:non
output:_resizeFactor
*/
int Vector::resizeFactor()const
{
	return this->_resizeFactor;
}

/*Function return if the vector is empty
input:non
output:true-is empty else false
*/
bool Vector::empty() const
{
	/*if the size  = 0 the function return false and if the size >0 the function will return true*/
	return this->_size;
}

/*Function adds element at the end of the vector
input:val-value to add
output:non
*/
void Vector::push_back(const int& val)
{
	int * newPtr = 0;
	/*check if the vector is'nt full*/
	if (this->_size < this->_capacity)
	{
		/*add the value to the end of the arr*/
		this->_elements[this->_size] = val;
		this->_size = this->_size + 1;
	}
	else
	{
		/*creat new arr with the new size*/
		newPtr = (int*)malloc(sizeof(int) * (this->_capacity+this->_resizeFactor));
		/*copy the values from elements to the new arr*/
		for (int i = 0; i < this->_size; i++)
		{
			newPtr[i] = this->_elements[i];
		}
		/*change the capacity value to the current arr len*/
		this->_capacity = this->_capacity + this->_resizeFactor;
		/*the value to the end of the arr*/
		newPtr[this->_size] = val;
		this->_size = this->_size + 1;
		/*delete the dynamic memory from the arr*/
		delete[] this->_elements;
		/*set the elements pointer to the new arr*/
		this->_elements = newPtr;
	}
}
/*
Function delete the last accessible organ int the vector
intput:non
output:the value of the last  last accessible organ
*/
int Vector::pop_back()
{
	int rValue = 0;
	/*check if the array isn't empty*/
	if (this->_size != 0)
	{
		/*save the value of the last index*/
		rValue = this->_elements[this->_size-1];
		/*delete the last index and reseting the size*/
		this->_elements[this->_size - 1] = 0;
		this->_size = this->_size - 1;
		/*return the value*/
		return rValue;
	}
	else
	{
		rValue = ERORR_VALUE;
		cout << "erorr:pop from empy vector" << endl;
		return rValue;
	}
}

/*Function change the capacity of vectort to be at list len of n
input:n-len
output:non
*/
void Vector::reserve(int n)
{
	int multBy = 1;
	/*check if n bigger that the size rigth now*/
	if (n > this->_capacity)
	{
		/*check how much memory we need to allocate*/
		while (this->_resizeFactor*multBy < n-this->_capacity)
		{
			multBy = multBy + 1;
		}
		/*allocate the memory*/
		this->_elements = (int*)realloc(this->_elements, sizeof(int) * ((this->_resizeFactor*multBy)+ this->_capacity));
		/*change the capacity values*/
		this->_capacity = ((this->_resizeFactor*multBy) + this->_capacity);
	}
}

/*Function change _size to n, unless n is greater than the vector's capacity
input:n -size
output -non
*/
void Vector::resize(int n)
{
	/*check if  n is bigget(that we need to use the function reserve to increse the size of then arr)*/
	if (n > this->_capacity)
	{
		/*change the size to n*/
		reserve(n);
		/*set  0 in  to the new cells*/
		for (int i = this->_size; i < this->_capacity; i++)
		{
			this->_elements[i] = 0;
		}
		/*change the size*/
		this->_size = this->_capacity;
		
	}
	else 
	{
		while (n<this->_capacity)
		{
			this->_capacity = this->_capacity - 1;
		}
		/*alloc new size to the arr*/
		this->_elements = (int*)realloc(this->_elements, sizeof(int)*n);
		/*change the size*/
		this->_size = n;
	
	}
}

/*Function  assigns val to all elemnts
input:val
output:non
*/
void Vector::assign(int val)
{
	/*go over the elements and set the value*/
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
	
} 



void Vector::resize(int n, const int& val)
{
	/*same as resize(int n) just add value to the new cells*/
	if (n > this->_capacity)
	{
		
		reserve(n);
		
		for (int i = this->_size; i < this->_capacity; i++)
		{
			/*add the value*/
			this->_elements[i] = val;
		}
		
		this->_size = this->_capacity;

	}
	else
	{
		while (n < this->_capacity)
		{
			this->_capacity = this->_capacity - 1;
		}
		/*alloc new size to the arr*/
		this->_elements = (int*)realloc(this->_elements, sizeof(int)*n);
		/*change the size*/
		this->_size = n;

	}
	
}



int& Vector::operator[](int n) const
{
	if (n > this->_size)
	{
		cout << "n is beyond the limits of the vector " << endl;
		return this->_elements[0];
	}
	else
	{
		return this->_elements[n];
	}
}


Vector::Vector(const Vector& other)
{
	
	this->_elements = (int*)malloc(sizeof(int)*other._capacity);
	/*copy the elements*/
	for (int i = 0; i < other._size; i++)
	{
		this->_elements[i] = other._elements[i];
	}

	/*set the argument valus*/
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;
	this->_capacity = other._capacity;
	
}


Vector&  Vector::operator = (const Vector& other)
{
	this->_elements = (int*)realloc(this->_elements,sizeof(int)*other._capacity);
	/*copy the elements*/
	for (int i = 0; i < other._size; i++)
	{
		this->_elements[i] = other._elements[i];
	}

	/*set the argument valus*/
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;
	this->_capacity = other._capacity;
	return *this;
}
